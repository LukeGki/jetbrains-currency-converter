import requests


class CurrencyConverter:
    def __init__(self):
        self.exchange_rates = None
        self.run()

    def read_exchange_rate_from_exchange_rates(self, currency):
        try:
            return self.exchange_rates[f"{currency.lower()}"]["rate"]
        except KeyError:
            pass

    def save_exchange_rate_to_cache(self, currency, exchange_rate):
        with open("cache.txt", "a") as cache:
            cache.write(currency.upper() + ": " + str(exchange_rate) + "\n")

    def is_currency_in_cache(self, currency):
        exchange_rate = None
        with open("cache.txt", "r") as cache:
            for line in cache:
                if currency.upper() in line:
                    exchange_rate = float(line.split(" ")[1])
                    break
        return exchange_rate

    def run(self):
        currency_from = input()

        self.exchange_rates = requests.get(f"http://www.floatrates.com/daily/{currency_from.lower()}.json").json()
        self.save_exchange_rate_to_cache("USD", self.read_exchange_rate_from_exchange_rates("USD"))
        self.save_exchange_rate_to_cache("EUR", self.read_exchange_rate_from_exchange_rates("EUR"))

        while True:
            currency_to = input()

            if currency_to == "":
                break

            currency_from_value = float(input())

            print("Checking the cache...")

            exchange_rate = self.is_currency_in_cache(currency_to)
            if exchange_rate:
                print("Oh! It is in the cache!")
                currency_to_value = currency_from_value * exchange_rate
            else:
                print("Sorry, but it is not in the cache!")
                exchange_rate = self.read_exchange_rate_from_exchange_rates(currency_to)
                self.save_exchange_rate_to_cache(currency_to, exchange_rate)
                currency_to_value = float(currency_from_value) * exchange_rate

            print(f"You received {round(currency_to_value, 2)} {currency_to}.")


program = CurrencyConverter()
